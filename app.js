const createError = require('http-errors');
const express = require('express');
const path = require('path');

const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const chatRouter = require('./routes/chat');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// The session configuration
app.use(session({
    secret: '3006A2 Project',
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 1000 * 60 * 60}// Specifies the valid length of the login session (60 minutes)
}))

// Login interception (session timeout will prevent access to the home page and redirect to the login page)
app.get('*', function (req, res, next) {
    var username = req.session.username
    var path = req.path
    // console.log('session', username)
    if (path != '/login' && path != '/register') {
        if (!username) {
            res.send('<script>alert("No login, please login in first !"); location.href="/login"</script>')
            return
        }
    }
    next()
})

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/chat', chatRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
// http.listen('3000','http://127.0.0.1/')