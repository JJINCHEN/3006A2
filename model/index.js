var MongoClient = require('mongodb').MongoClient;

var url = 'mongodb://localhost:27017';
var dbname = "3006A2"

//Database connection method encapsulation
function connect(callback) {
    MongoClient.connect(url, function (err, client) {
        if (err) {
            console.log("db connect error", err)
        } else {
            // console.log("model db connect successful")
            var db = client.db(dbname)
            callback && callback(db)
            // client.close()
        }
    })
}

module.exports = {connect}