const express = require('express');
const router = express.Router();
const model = require('../model');
const url = require('url')

var app = express();

var http = require("http");


/* GET users listing. */
// Chat room message capture
router.get('/room1', function (req, res, next) {
    // Connect database
    model.connect(function (db) {
        // Connect database to tack out the chat room name
        db.collection('chatname').find().toArray(function (err, docs) {
            if (err) {
                console.log("db connect error", err)
            } else {
                var chatname = docs
                // console.log("chatname list: ", chatname)
                var username = req.session.username
                // console.log(username)
                var room = '1'
                // Connect database to take out all messages in this chat room
                db.collection('chat').find({roomid: '1'}).toArray(function (err, docs) {
                    if (err) {
                        console.log("db connect error", err)
                    } else {
                        // Take all message in variable
                        var chat = docs
                        // console.log("chat list: ", chat)
                        // Transfer the data to the view
                        res.render('chat', {username: username, chatname: chatname, room: room, chat: chat});
                        return
                    }
                })
                // res.render('chat', {username: username, chatname:chatname, room:room});
                // return
            }
        })

    })
});

// Chat room message capture
router.get('/room2', function (req, res, next) {
    // Connect database
    model.connect(function (db) {
        // Connect database to tack out the chat room name
        db.collection('chatname').find().toArray(function (err, docs) {
            if (err) {
                console.log("db connect error", err)
            } else {
                var chatname = docs
                // console.log("chatname list: ", chatname)
                var username = req.session.username
                // console.log(username)
                var room = '2'
                // Connect database to take out all messages in this chat room
                db.collection('chat').find({roomid: '2'}).toArray(function (err, docs) {
                    if (err) {
                        console.log("db connect error", err)
                    } else {
                        // Take all message in variable
                        var chat = docs
                        // console.log("chat list2: ", chat)
                        // Transfer the data to the view
                        res.render('chat', {username: username, chatname: chatname, room: room, chat: chat});
                        return
                    }
                })
                // res.render('chat', {username: username, chatname:chatname, room:room});
                // return
            }
        })

    })
});

// Chat room message capture
router.get('/room3', function (req, res, next) {
    // Connect database
    model.connect(function (db) {
        // Connect database to tack out the chat room name
        db.collection('chatname').find().toArray(function (err, docs) {
            if (err) {
                console.log("db connect error", err)
            } else {
                var chatname = docs
                // console.log("chatname list: ", chatname)
                var username = req.session.username
                console.log(username)
                var room = '3'
                // Connect database to take out all messages in this chat room
                db.collection('chat').find({roomid: '3'}).toArray(function (err, docs) {
                    if (err) {
                        console.log("db connect error", err)
                    } else {
                        // Take all message in variable
                        var chat = docs
                        // console.log("chat list3: ", chat)
                        // Transfer the data to the view
                        res.render('chat', {username: username, chatname: chatname, room: room, chat: chat});
                        return
                    }
                })
                // res.render('chat', {username: username, chatname:chatname, room:room});
                // return
            }
        })

    })
});

// Method of sending a message to write to a database
router.post('/send', function (req, res, next) {
    var data = {
        room: req.body.room,
        message: req.body.message
    }
    var date = new Date()
    var username = req.session.username
    var senddb = {
        roomid: req.body.room,
        from: username,
        message: req.body.message,
        time: date
    }
    // console.log(senddb)
    if (!senddb.from) {
        // Return to login if there is no session username
        res.send('<script>alert("No login, please login in first!"); location.href="/login"</script>')
        return
    } else {
        if (!senddb.message) {
            // Send an empty message directly back to the chat page
            var reurl = 'room' + data.room
            res.redirect(reurl)
        } else {
            // Connect database
            model.connect(function (db) {
                // console.log("4")
                // Writes the sent message information to the database
                db.collection('chat').insertOne(senddb, function (err, ret) {
                    if (err) {
                        console.log('Senddb error-2')
                        return
                    } else {
                        console.log('Send successful-1')
                        var reurl = 'room' + data.room
                        res.redirect(reurl)
                    }
                })
            })
        }
    }
})


module.exports = router;