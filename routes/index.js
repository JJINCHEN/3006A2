const express = require('express');
const router = express.Router();
const model = require('../model')


/* GET index page. */
router.get('/', function (req, res, next) {
    var username = req.session.username
    model.connect(function (db) {
        db.collection('users').find().toArray(function (err, docs) {
            if (err) {
                console.log("db connect error", err)
            } else {
                // console.log("user list: ", docs)
                res.render('index', {username: username});
                return
            }
        })
    })
});

// Render registration page
router.get('/register', function (req, res, next) {
    res.render('register', {})
    return
})
// Render login page
router.get('/login', function (req, res, next) {
    res.render('login', {})
    return
})
// Render modify page
router.get('/modify', function (req, res, next) {
    var username = req.session.username
    res.render('change', {username: username})
    return
})

module.exports = router;
