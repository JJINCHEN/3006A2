const express = require('express');
const router = express.Router();
const model = require('../model');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
    return
});

// Register interface
router.post('/register', function (req, res, next) {
    console.log("1")
    var data = {
        username: req.body.username,
        password: req.body.password,
        repassword: req.body.repassword
    }
    //Data verification
    if (!data.username) {
        res.send('<script>alert("Username is null! "); location.href="/register"</script>')
        return
    }
    else if(!data.password || !data.repassword){
        res.send('<script>alert("Password or Re-password is null! "); location.href="/register"</script>')
        return
    }else {
        // Database connect
        model.connect(function (db) {
            console.log("2")
            // Check whether the user name exists
            db.collection('users').find({username: data.username}).toArray(function (err, docs) {
                // console.log('111',docs)
                if (err) {
                    console.log('Register error')
                    res.redirect('/register')
                    return
                } else {
                    // console.log("username same list: ", docs)
                    // If the user name is prompted and re-registered
                    if (docs.length > 0) {
                        res.send('<script>alert("User name already exists"); location.href="/register"</script>')
                        return
                    }
                    // Register the user if the user name does not exist
                    else {
                        if (data.password != data.repassword) {
                            res.send('<script>alert("The passwords entered twice are different"); location.href="/register"</script>')
                            return
                        } else {
                            // Writes user information to the database
                            db.collection('users').insertOne(data, function (err, ret) {
                                if (err) {
                                    console.log('Register error')
                                    res.send('<script>alert("Register error"); location.href="/register"</script>')
                                    return
                                } else {
                                    console.log('Register successful')
                                    res.send('<script>alert("Register successful! Login NOW! "); location.href="/login"</script>')
                                    return
                                }
                            })
                        }
                    }
                }
            })
        })
        // res.send(data)
    }
})

// Login interface
router.post('/login', function (req, res, next) {
    var data = {
        username: req.body.username,
        password: req.body.password
    }
    // console.log(data)
    //Data verification
    if (!data.username || !data.password) {
        res.send('<script>alert("User name or password is null, please re-login");location.href="/login"</script>')
        return
    } else {
        // Connect database
        model.connect(function (db) {
            // Check whether the user name exists
            db.collection('users').find({username: data.username}).toArray(function (err, docs) {
                if (err) {
                    console.log(err)
                    res.send('<script>alert("Some error return to modify now"); location.href="/modify"</script>')
                    return
                } else {
                    if (docs.length > 0) {
                        // Find the corresponding content
                        db.collection('users').find(data).toArray(function (err, docs) {
                            if (err) {
                                console.log(err)
                                res.send('<script>alert("Some error return to login now"); location.href="/login"</script>')
                                return
                            } else {
                                // console.log("user list: ", docs)
                                if (docs.length > 0) {
                                    //Session storage is performed after the login succeeds
                                    req.session.username = data.username
                                    res.send('<script>alert("Login successful! "); location.href="/chat/room1" </script>')
                                    return
                                } else {
                                    res.send('<script>alert("Password error, Please re-login"); location.href="/login"</script>')
                                    return
                                }
                            }
                        })
                    }else {
                        res.send('<script>alert("Not have account yet, Register Now"); location.href="/register"</script>')
                        return
                    }
                }})
        })
    }
})
//Log out
router.get('/logout', function (req, res, next) {
    // Clear session
    req.session.username = null
    res.send('<script>alert("Logout successful! "); location.href="/login" </script>')
    return
})

// Method of changing the password to the database, modify interface
router.post('/modify', function (req, res, next) {
    var data = {
        username: req.session.username,
        password: req.body.password,
        repassword: req.body.repassword
    }
    // console.log(data)
    // Data verification
    // First determine if it is completely filled in
    if (!data.username || !data.password ||!data.repassword) {
        res.send('<script>alert("User name or password is null");location.href="/modify"</script>')
        return
    } else {
        // Check whether the two passwords are the same
        if (data.password != data.repassword) {
            res.send('<script>alert("The passwords entered twice are different"); location.href="/modify"</script>')
            return
        } else {
            // Connect database
            model.connect(function (db) {
                // Check whether the user name exists
                db.collection('users').find({username: data.username}).toArray(function (err, docs) {
                    if (err) {
                        console.log(err)
                        res.send('<script>alert("Some error, return to modify now"); location.href="/modify"</script>')
                        return
                    } else {
                        if (docs.length > 0) {
                            // Search for the user name and password to change the password
                            db.collection('users').updateOne(
                                {username: data.username},
                                {
                                    $set: {
                                        password: data.password,
                                        repassword: req.body.repassword
                                    }
                                }, function (err, ret) {
                                    if (err) {
                                        console.log('Modify error')
                                        res.send('<script>alert("Modify error"); location.href="/modify"</script>')
                                        return
                                    } else {
                                        console.log('Modify successful')
                                        res.send('<script>alert("Modify successful! Chat NOW! "); location.href="/chat/room1"</script>')
                                        return
                                    }
                                })
                        } else {
                            res.send('<script>alert("Some error return to modify now"); location.href="/modify"</script>')
                        }
                    }
                })
            })
        }
    }
})
module.exports = router;
